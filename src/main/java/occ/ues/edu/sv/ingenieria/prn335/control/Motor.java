/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package occ.ues.edu.sv.ingenieria.prn335.guia01.control;

import java.util.ArrayList;



import occ.ues.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author Esperanza
 */
public class Motor {

    ArrayList<Sucursal> sucursales = new ArrayList<Sucursal>();
   
    public Motor() {
        sucursales.add(new Sucursal(1, "Cinepolis Metrocentro", "Santa Ana", "Santa Ana", "Juan Perez", true));
        sucursales.add(new Sucursal(2, "Cinepolis La Gran Via", "Antiguo Cuscatlan", "San Salvador", "Luis Lopez", true));
        sucursales.add(new Sucursal(3, "Cinepolis Metrocento SM", "San Miguel", "San Miguel", "Will Salgado", false));
        sucursales.add(new Sucursal(4, "Cinemark Metrocentro", "Soyapango", "San Salvador", "Encargado 1", false));
        sucursales.add(new Sucursal(5, "Cinepolis Metrocentro Sonso", "Sonsonate", "Sonsonate", "Encargado 2", true));
        sucursales.add(new Sucursal(6, "Cine La Union", "Puerto de la union", "La Union", "Encargado 3", true));
        sucursales.add(new Sucursal(7, "Cine La Union 2", "Ciudad de la union", "La Union", "Encargado 3", false));
    }
  
public ArrayList<Sucursal> mostrarSucursales() {return sucursales;}
 
}
